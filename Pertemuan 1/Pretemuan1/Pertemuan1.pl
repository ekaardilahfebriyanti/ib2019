male(budi).
male(nurdin).
female(ayu).
female(cici).
man(zauhari).
woman(sahira).
suka(eka,buah).
%nama, tinggi, kota, status
person(budi,160,bandung,jomlo).
person(nurdin,170,jakarta,menikah).
person(ayu,150,medan,jomlo).
person(shafira,155,jogjakarta,menikah).
person(evi,150,bandung,bukacabang).



%ntuk chek lokasi
located_in(atlanta,georgia).
located_in(houston,texas).
located_in(austin,texas).
located_in(toronto,ontario).
located_in(X,usa) :- located_in(X,georgia).
located_in(X,usa) :- located_in(X,texas).
located_in(X,canada) :- located_in(X,ontario).
located_in(X,north_america) :- located_in(X,usa).
located_in(X,north_america) :- located_in(X,canada).
